# ChannelsProject2.0

Simple demo app for getting a list of TV channels.

In this example you can see the following:

*  Clean architecture
*  MVVM Android architecture
*  Getting data from a JSON file saved in assets


