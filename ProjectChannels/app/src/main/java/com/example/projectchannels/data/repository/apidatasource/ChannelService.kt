package com.example.projectchannels.data.repository.apidatasource


import com.example.projectchannels.data.model.dto.ChannelDTO
import com.google.gson.Gson

class ChannelService {

    private val gson by lazy { Gson() }

    private fun deserializeFromJson(jsonString: String?): ChannelDTO? {
        return gson.fromJson<ChannelDTO>(jsonString, ChannelDTO::class.java)
    }

    fun getChannelsFromJson(
        jsonData: String?,
        failure: (Error) -> Unit,
        success: (ChannelDTO) -> Unit
    ) {
        try {
            val channelDTO = deserializeFromJson(jsonData)?:ChannelDTO()
            success(channelDTO)

        } catch (e: Exception) {
            failure(java.lang.Error())
        }
    }

}