package com.example.projectchannels.view.channel.adapter

import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.projectchannels.domain.bo.ResponseBO
import com.example.projectchannels.domain.vo.ResponseVO
import com.example.projectchannels.view.channel.fragment.ChannelsFragment


class ChannelViewPagerAdapter(manager: FragmentManager, val data: ArrayList<ResponseVO>) :
    FragmentPagerAdapter(manager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {

        return when (position) {

            0 -> {
                ChannelsFragment.newInstance(data, false)
            }

            1 -> {
                ChannelsFragment.newInstance(data, true)
            }
            else -> {
                ChannelsFragment.newInstance(data, false)
            }
        }
    }

    override fun getCount(): Int {
        return 2
    }
}