package com.example.projectchannels.view.channel.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.projectchannels.R
import com.example.projectchannels.domain.bo.ResponseBO
import com.example.projectchannels.domain.vo.ResponseVO
import com.example.projectchannels.utils.Constants.Companion.BASE_URL_iMAGE
import com.example.projectchannels.utils.ImageUtils
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_channels.view.*

class ChannelRecyclerAdapter(private var list: List<ResponseVO>) :
    RecyclerView.Adapter<ChannelRecyclerAdapter.ChannelsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChannelsViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_channels, parent, false)
        return ChannelsViewHolder(v)
    }

    override fun onBindViewHolder(holder: ChannelsViewHolder, position: Int) {
        holder.setMediaData(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class ChannelsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun setMediaData(data: ResponseVO?) {
            var endImageUrl = ""
            var urlImageFinal = ""
            itemView.tvChannels.text = data?.name
            if (data?.attachments?.isNullOrEmpty() == false) {
                endImageUrl = data.attachments.first().value.toString()
                urlImageFinal = BASE_URL_iMAGE + endImageUrl
            }

            if (endImageUrl.isEmpty()) {
                itemView.ivChannels.setImageResource(R.drawable.empty)
            } else {
                ImageUtils.drawImage(urlImageFinal, itemView.ivChannels)
            }
        }
    }


}