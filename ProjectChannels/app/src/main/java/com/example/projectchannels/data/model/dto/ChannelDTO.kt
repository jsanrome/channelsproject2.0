package com.example.projectchannels.data.model.dto

import com.example.projectchannels.data.base.BaseDTO
import com.google.gson.annotations.SerializedName

data class ChannelDTO(
    @SerializedName("metadata")
    val metadata: MetadataDTO?=null,
    @SerializedName("response")
    val response: List<ResponseDTO>?=null
):BaseDTO()