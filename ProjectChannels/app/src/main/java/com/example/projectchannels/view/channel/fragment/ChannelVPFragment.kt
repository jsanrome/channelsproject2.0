package com.example.projectchannels.view.channel.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.projectchannels.R
import com.example.projectchannels.domain.bo.ResponseBO
import com.example.projectchannels.domain.vo.ResponseVO
import com.example.projectchannels.view.channel.adapter.ChannelViewPagerAdapter
import com.example.projectchannels.view.channel.viewmodel.ChannelsViewModel
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_channel_v.*
import java.io.IOException
import java.io.InputStream
import java.nio.charset.Charset


class ChannelVPFragment : Fragment() {
    private var adapterViewPager: ChannelViewPagerAdapter? = null
    private lateinit var viewModel: ChannelsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_channel_v, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ChannelsViewModel::class.java)
        initObservers()
        initListener()
        initTabLayout()
        getJSONContentFromFile()
    }


    private fun getJSONContentFromFile() {
        viewModel.onSendJSONContent(loadJSONFromAsset() ?: "")
    }

    private fun initListener() {}

    private fun initObservers() {
        viewModel.ldChannels.observe(this, Observer { data ->
            data?.let {
                initViewPagerAdapter(data.response as ArrayList<ResponseVO>)
            }
        })
    }

    private fun loadJSONFromAsset(): String? {
        val json: String?

        json = try {

            val `is`: InputStream? = context?.assets?.open("data.json")
            val size: Int? = `is`?.available()
            val buffer = size?.let { ByteArray(it) }
            `is`?.read(buffer)
            `is`?.close()
            buffer?.let {
                String(
                    it, Charset.forName("UTF-8")
                )
            }
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }

    private fun initTabLayout() {
        tabLayout.addTab(tabLayout.newTab().setText("TODOS"))
        tabLayout.addTab(tabLayout.newTab().setText("CINE"))

        tabLayout.tabGravity = TabLayout.GRAVITY_FILL


        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPagerChannel.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

    }

    private fun initViewPagerAdapter(response: ArrayList<ResponseVO>?) {

        childFragmentManager.let { fragmentManager ->
            adapterViewPager = response?.let { ChannelViewPagerAdapter(fragmentManager, it) }
        }

        viewPagerChannel.adapter = adapterViewPager
        viewPagerChannel.offscreenPageLimit = response?.size as Int

        viewPagerChannel.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
    }
}
