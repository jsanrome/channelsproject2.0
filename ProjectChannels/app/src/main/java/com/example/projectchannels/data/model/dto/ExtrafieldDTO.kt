package com.example.projectchannels.data.model.dto

import com.example.projectchannels.data.base.BaseDTO
import com.google.gson.annotations.SerializedName

data class ExtrafieldDTO(
    @SerializedName("name")
    val name: String?= null,
    @SerializedName("responseElementType")
    val responseElementType: String?= null,
    @SerializedName("value")
    val value: String?= null
) : BaseDTO()