package com.example.projectchannels.domain.mappers

import com.example.projectchannels.domain.bo.*
import com.example.projectchannels.domain.vo.*

class MapperChannelBOToVO {
    companion object {

        fun convertChannelBOToVO(model: ChannelBO?) =
            ChannelVO(
                metadata = convertMetadataToVO(model?.metadata),
                response = convertListResponseTOVO(model?.response)
            )

        fun convertMetadataToVO(model: MetadataBO?) =
            MetadataVO(model?.fullLength, model?.request, model?.timestamp)

        fun convertResponseToVO(model: ResponseBO?) =
            ResponseVO(name = model?.name, attachments = convertListAttachmentToVO(model?.attachments), extrafields = convertListExtrafielToVO(model?.extrafields))

        fun convertListResponseTOVO(model: List<ResponseBO>?): List<ResponseVO> {

            val listResponseVO = arrayListOf<ResponseVO>()
            model?.forEach {
                listResponseVO.add(convertResponseToVO(it))
            }
            return listResponseVO
        }

        fun convertAttachmentToVO(model: AttachmentBO?) = AttachmentVO(
            model?.responseElementType,
            model?.assetId,
            model?.name,
            model?.assetName,
            model?.value
        )
        fun convertAttachmentToVO(model: ExtrafieldBO?) = ExtrafieldVO(
            model?.responseElementType,
            model?.name,
            model?.value
        )

        fun convertListAttachmentToVO(model: List<AttachmentBO>?): List<AttachmentVO> {
            val attachmentVO = arrayListOf<AttachmentVO>()
            model?.forEach {
                attachmentVO.add(convertAttachmentToVO(it))
            }
            return attachmentVO
        }
        fun convertListExtrafielToVO(model: List<ExtrafieldBO>?): List<ExtrafieldVO> {
            val extrafieldListVO = arrayListOf<ExtrafieldVO>()
            model?.forEach {
                extrafieldListVO.add(convertAttachmentToVO(it))
            }
            return extrafieldListVO
        }

    }
}