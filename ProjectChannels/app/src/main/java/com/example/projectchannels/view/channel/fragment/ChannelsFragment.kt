package com.example.projectchannels.view.channel.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.projectchannels.R
import com.example.projectchannels.domain.vo.ResponseVO
import com.example.projectchannels.view.channel.adapter.ChannelRecyclerAdapter
import com.example.projectchannels.view.channel.viewmodel.ChannelsViewModel
import kotlinx.android.synthetic.main.channels_fragment.*


class ChannelsFragment : Fragment() {
    private var ResponseVO: List<ResponseVO>? = null
    private var isCine = false

    companion object {
        const val EXTRA_RESPONSE: String = "extra_response"
        const val EXTRA_IS_CINE: String = "is_cine"

        fun newInstance(dataList: ArrayList<ResponseVO>, isCine: Boolean): ChannelsFragment {
            val fragment = ChannelsFragment()
            val args = Bundle()
            args.putSerializable(EXTRA_RESPONSE, dataList)
            args.putSerializable(EXTRA_IS_CINE, isCine)
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var viewModel: ChannelsViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.channels_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ChannelsViewModel::class.java)
        var ResponseVOFinal : List<ResponseVO>? = null

        arguments?.let { args ->
            ResponseVO = args.getSerializable(EXTRA_RESPONSE) as List<ResponseVO>
            isCine = args.getSerializable(EXTRA_IS_CINE) as Boolean
            if (isCine) {
                ResponseVOFinal = ResponseVO?.filter { response ->
                    response.extrafields!!.any { extrafield ->
                        extrafield.value == "NRS_CINE_Y_SERIES"
                    }
                }
            } else
                ResponseVOFinal = ResponseVO
        }

        initRecycler(ResponseVOFinal)
    }

    private fun initRecycler(data: List<ResponseVO>?) {
        data?.sortedByDescending { it.name }
        val linearLayoutManager = activity?.let {
            LinearLayoutManager(it).apply {
                orientation = RecyclerView.VERTICAL
            }
        }
        val adapterRecycler = data?.let { ChannelRecyclerAdapter(it) }

        rvChannels.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            adapter = adapterRecycler

        }

    }


}
